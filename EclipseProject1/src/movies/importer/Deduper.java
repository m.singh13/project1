package movies.importer;

import java.util.ArrayList;
/**
 *  Removes duplicates by merging them into a single entry
 *  @author Mandeep Singh Mishal Alarifi
 *
 */
public class Deduper extends Processor{
	
	public Deduper(String sourceDirectory,String destinationDirectory) {
		super(sourceDirectory,destinationDirectory,false);
	}
	
	/** method which removes any duplicates from the input ArrayList<String> and merges them into a single entry
	 * @param input An ArrayList<String> representing the input which is now processed data
	 * @return Returns an ArrayList<String> representing data with all duplicates merged into one entry
	 */
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<Movie> movies = Utilities.convertToMovie(input);
		ArrayList<Movie> withoutDuplicate = new ArrayList<Movie>();
		//for each movie in the ArrayList it checks if it is in the ArrayList already and if it is it merges if not it adds it
		for(Movie film : movies) {
			if(!withoutDuplicate.contains(film)) {
				withoutDuplicate.add(film);
			}
			else {
				mergeMovies(withoutDuplicate, film);
			}
		}
		return Utilities.convertMovieToInfo(withoutDuplicate);
	}
	
	/** abstract method that is required to be provided in any concrete class that
	 * extends the abstract Processor class
	 * @param withoutDuplicate An ArrayList<Movie> representing the data in the form of many Movie objects
	 * @param film Movie representing the movie at the current position of the given ArrayList<Movie>
	 */
	private void mergeMovies(ArrayList<Movie> withoutDuplicate, Movie film) {
		int i = withoutDuplicate.indexOf(film);
		Movie duplicate = withoutDuplicate.get(i);
		//gets the larger of the two runtimes
		int maxRuntime =  Math.max(Integer.parseInt(duplicate.getRuntime()), Integer.parseInt(film.getRuntime()));
		withoutDuplicate.set(i, new Movie(film.getReleaseYear(), film.getName(), Integer.toString(maxRuntime)));
		Movie mergedMovie = withoutDuplicate.get(i);
		//if the sources are the same the new entry will keep that source otherwise it gets IMDB:Kaggle
		if(duplicate.getSource().equals(film.getSource())){
			mergedMovie.setSource(film.getSource());
		}
		else {
			mergedMovie.setSource("IMDB:Kaggle");
		}
	}
}
