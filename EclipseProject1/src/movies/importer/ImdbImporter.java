package movies.importer;

import java.util.ArrayList;

/**
 * This class takes the raw data from files and takes just the key qualities
 * @author Mishal Alarifi
 *
 */

public class ImdbImporter extends Processor{
	//column of the data where year is represented
	private final int YEARCOL = 4;
	//column of the data where title is represented
	private final int TITLECOL = 2;
	//column of the data where runtime is represented
	private final int RUNTIMECOL = 7;
	//the number of columns a row should have
	private final int NUMCOL = 22;
	
	public ImdbImporter(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}
	
	/** method which takes just the year, title, and runtime from file
	 * @param input An ArrayList<String> representing the raw input from the file
	 * @return Returns an ArrayList<String> representing the most important attributes from the raw data
	 */
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<Movie> movies = Utilities.convertToMovie(input , YEARCOL, TITLECOL, RUNTIMECOL, "IMDB", NUMCOL);
		return Utilities.convertMovieToInfo(movies);
	}
}
