package movies.importer;

/**
 * This class is part of the Movie object
 * @author Mandeep Singh Mishal Alarifi
 *
 */

public class Movie {
	//Private fields
	private String releaseYear;
	private String name;
	private String runtime;
	private String source;
	
	
	//Movie constructor with 3 parameters
	public Movie(String year,String name,String runtime) {
		this.releaseYear = year;
		this.name = name;
		this.runtime = runtime;
		
	}
	//Movie constructor with no parameters
	public Movie() {
	}
	
	//getters
	public String getReleaseYear() {return this.releaseYear;}
	public String getName() {return this.name;}
	public String getRuntime() {return this.runtime;}
	public String getSource() {return this.source;}
	//setters
	public void setSource(String source) {this.source = source;}
	//Overriding the toString method
	public String toString() {return( getName() + "\t" + getReleaseYear()    + "\t" +getRuntime() + "\t" +  getSource());}
	//Overriding the equals method
	public boolean equals(Object obj){
		Movie otherMovie = (Movie) obj;
		int difference = Integer.parseInt(this.runtime) - Integer.parseInt(otherMovie.runtime);
		if(this.name.equals(otherMovie.name) && this.releaseYear.equals(otherMovie.releaseYear) &&  difference <= 5 && difference >= -5){
			return true;
		}
		return false;
	}
}
