package movies.importer;
import java.util.*;
/**
 * 
 * @author Mandeep Singh
 * 
 * This class transforms the raw RottenTomatoes file into the standard format
 *
 */
public class KaggleImporter extends Processor{
	//Constant variables reprensenting the columns of the necessary information
	private final int MAX_COLUMNS =21;
	private final int TITLE_COLUMN =16;
	private final int RUNTIME_COLUMN=14;
	private final int YEAR_COLUMN=21;
	
	//Constructor 
	public KaggleImporter(String sourceDirectory,String destinationDirectory) {
		super(sourceDirectory,destinationDirectory,true);
	}

	/**
	 * 
	 *@param input An ArrayList<String> representing the raw textfile
	 *@return An ArrayList<String> representing the proper format of the movies
	 */
	public ArrayList<String> process(ArrayList<String> input){
		
		ArrayList <Movie> movies = Utilities.convertToMovie(input, YEAR_COLUMN, TITLE_COLUMN, RUNTIME_COLUMN, "Kaggle",MAX_COLUMNS);
		return  Utilities.convertMovieToInfo(movies);
	}
	
	

}
