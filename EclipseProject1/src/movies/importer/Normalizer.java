package movies.importer;

import java.util.ArrayList;

/**
 * 
 * @author Mandeep Singh
 * 
 * This class normalizes takes the standardFormat of the movies and normalizes them
 */
public class Normalizer extends Processor {
	//Constant variables
	private final int MAX_COLUMNS =4;
	private final int TITLE_COLUMN =0;
	private final int RUNTIME_COLUMN=2;
	private final int YEAR_COLUMN=1;
	private final int SOURCE_COLUMN=3;
	//Constructor
	public Normalizer(String sourceDirectory,String destinationDirectory) {
		super(sourceDirectory,destinationDirectory,false);
	}
	/**
	 * @param input An ArrayList<String> representing the standardFormat file
	 * @return An ArrayList <String> representing the normalized format of the movies
	 */
	public ArrayList<String> process(ArrayList<String> input){
		//Initializing a Movie and String ArrayList
		ArrayList<Movie> movies = new ArrayList<Movie>();
		ArrayList<String> normalizedString = new ArrayList<String>();
		//j counter that increments only when proper amount of columns
		int j =0;
		//Creates  the movies in normalized form
		for(int i =0; i < input.size(); i++) {
			String[] moviesAttributes = input.get(i).split("\\t",-1);
			if(moviesAttributes.length == MAX_COLUMNS) {
				String [] runtimeString = moviesAttributes[RUNTIME_COLUMN].split(" ");
				movies.add(new Movie(moviesAttributes[YEAR_COLUMN],moviesAttributes[TITLE_COLUMN].toLowerCase(),runtimeString[0]));
				movies.get(j).setSource(moviesAttributes[SOURCE_COLUMN]);
				j++;
				}		
			}
		//Tranforms the movies into string
		for(int i =0;i<movies.size();i++) {
			normalizedString.add(movies.get(i).toString());
		}
			return normalizedString;
	}

}
