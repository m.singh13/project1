package movies.importer;

import java.util.ArrayList;
/**
 *  Collection of methods used in the Importer classes
 *  @author Mishal Alarifi
 *
 */

public class Utilities {
	
	/** method which converts a given ArrayList<String> to an ArrayList<Movie>
	 * @param input An ArrayList<String> representing the raw data from a file to converted to Movies
	 * @return Returns an ArrayList<Movie> representing the equivalent of the input as a collection of Movies
	 */
	public static ArrayList<Movie> convertToMovie(ArrayList<String> file , int yearColumn, int nameColumn, int runtimeColumn, String database, int numCol){
		ArrayList<Movie> movies = new ArrayList<Movie>();
		//j is a separate counter which is incremented only when a row has the correct number of columns
		int j = 0;
		for (int i = 0; i < file.size(); i++){ 
		    String[] movieAttributes = file.get(i).split("\\t",-1);
		    if(movieAttributes.length == numCol){
		    	movies.add(new Movie(movieAttributes[yearColumn-1], movieAttributes[nameColumn-1], movieAttributes[runtimeColumn-1]));
		    	movies.get(j).setSource(database);
		    	j++;
		    }	
		}
		return movies;
	}
	
	/** method which converts a processed ArrayList<String> to an ArrayList<Movie>
	 * @param input An ArrayList<String> representing the processed date to be converted to Movies
	 * @return Returns an ArrayList<Movie> representing the equivalent of the input as a collection of Movies
	 */
	public static ArrayList<Movie> convertToMovie(ArrayList<String> input){
		ArrayList<Movie> movies = new ArrayList<Movie>();
		for (int i = 0; i < input.size(); i++){ 
		    String[] movieAttributes = input.get(i).split("\\t",-1);
		    movies.add(new Movie(movieAttributes[1], movieAttributes[0], movieAttributes[2]));
		    movies.get(i).setSource(movieAttributes[3]);
		}
		return movies;
	}
	
	/** method which converts a collection of Movies to a collection of their String equivalents
	 * @param input An ArrayList<Movie> representing the collection of Movies to be converted
	 * @return Returns an ArrayList<String> representing the equivalent of the input as an ArrayList<String>
	 */
	public static ArrayList<String> convertMovieToInfo(ArrayList<Movie> movies){
		ArrayList<String> movieInfo = new ArrayList<String>();
		for (int i = 0; i < movies.size(); i++) {
			movieInfo.add(movies.get(i).toString());
		}
		return movieInfo;
	}
}
