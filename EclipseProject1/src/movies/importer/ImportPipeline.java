package movies.importer;

import java.io.IOException;

/**
 * 
 * @author Mandeep Singh Mishal Alarifi
 * This class executes all processors created
 */
public class ImportPipeline {
	
	public static void main(String[] args) throws IOException {
		
		//String variables of source directory and source output locations
		 String rawRottenFileLocation="C:\\Users\\mande\\Desktop\\rtFile";
		 String rawIMDBFileLocation="C:\\Users\\mande\\Desktop\\imdbFile";
		 String normalizerLocation="C:\\Users\\mande\\Desktop\\normalize";
		 String validatorLocation="C:\\Users\\mande\\Desktop\\validate";
		 String deduperLocation="C:\\Users\\mande\\Desktop\\dedupe";
		 String mergedLocation="C:\\Users\\mande\\Desktop\\merged";
		
		//Initialize all processors
		Processor[] processors = new Processor[5];
		processors[0] = new KaggleImporter(rawRottenFileLocation,normalizerLocation);
		processors[1] = new ImdbImporter(rawIMDBFileLocation,normalizerLocation);
		processors[2] = new Normalizer(normalizerLocation,validatorLocation);
		processors[3] = new Validator(validatorLocation,deduperLocation);
		processors[4] = new Deduper(deduperLocation,mergedLocation);
		
		//Execute all processors
		processAll(processors);
	}
	/**
	 * 
	 * @param processors 
	 * method executes all processors
	 * @throws IOException
	 */
	private static void processAll(Processor[] processors) throws IOException {
		
		for(int i =0; i<processors.length;i++) {
			processors[i].execute();
		}
		System.out.println("Processed!");
	}

}
