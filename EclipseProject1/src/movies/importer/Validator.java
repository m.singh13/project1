package movies.importer;

import java.util.ArrayList;
/**
 *  This class takes processed information and removes all entries which are invalid
 *  @author Mishal Alarifi
 *
 */
public class Validator extends Processor{
	public Validator(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}

	/** method which checks for invalid entries in the data and removes them
	 * @param input An ArrayList<String> representing the data with possibly invalid entries
	 * @return Returns an ArrayList<String> representing the data with all invalid entries removed
	 */
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<Movie> movies = Utilities.convertToMovie(input);
		int i = 0;
		while(i < movies.size()){ 
		    if(!validate(movies.get((i)))) {
		    	input.remove(i);
		    	movies.remove(i);
		    }
		    else {
		    	i++;
		    }
		}
		return input;
	}
	
	/** method which checks if a movie is valid based on if any of its attributes are null or empty Strings and if
	 * its ReleaseYear and Runtime are real numbers
	 * @param input a Movie representing the Movie to be validated
	 * @return Returns a boolean representing whether or not the Movie is valid
	 */
	private boolean validate(Movie movie) {
		if(movie.getName().equals("") || movie.getReleaseYear().equals("") || movie.getRuntime().equals("")) {
	    	return false;
	    }
	    else if(movie.getName().equals(null) || movie.getReleaseYear().equals(null) || movie.getRuntime().equals(null)) {
	    	return false;
	    }
	    try {
	    	Integer.parseInt(movie.getReleaseYear());
	    	Integer.parseInt(movie.getRuntime());
	    } 
	    catch (NumberFormatException e) {
	    	return false;
	    }
		return true;
	}
	
}
