package movies.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;


import java.io.IOException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import movies.importer.Normalizer;

/**
 * 
 * @author Mandeep Singh
 *
 */

class NormalizerTest {

	//Testing if process() returns an ArrayList<String> into the right format 
		@Test
		void testProcess() throws IOException {
			Normalizer test = new Normalizer("C:\\Users\\mande\\Desktop\\test1","C:\\Users\\mande\\Desktop\\test2");
			ArrayList<String> testInfo = new ArrayList<String>();
			ArrayList<String> testInfoHardcoded = new ArrayList<String>();
			testInfo.add("The Mummy: Tomb of the Dragon Emperor	2008	112 minutes	Kaggle");
			testInfoHardcoded.add("the mummy: tomb of the dragon emperor\t2008\t112\tKaggle");
			testInfo.add("The Masked Saint	2016	111 minutes	Kaggle");
			testInfoHardcoded.add("the masked saint\t2016\t111\tKaggle");
			testInfo.add("MOVIE TITLE	2020	60	Kaggle");
			testInfoHardcoded.add("movie title\t2020\t60\tKaggle");
			assertEquals(testInfoHardcoded,test.process(testInfo));
			
		}

}
