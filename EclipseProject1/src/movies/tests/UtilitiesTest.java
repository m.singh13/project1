package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.Movie;
import movies.importer.Utilities;

/**
 * 
 * @author Mishal Alarifi
 *
 */

class UtilitiesTest {

	@Test
	void testConvertToMovieWithParams() {
		ArrayList<String> info = new ArrayList<String>();
		ArrayList<Movie> infoVer = new ArrayList<Movie>();
		
		info.add("Shrek\t2001\t95\tIMDB");
		infoVer.add(new Movie("2001","Shrek","95"));
		infoVer.get(0).setSource("IMDB");
		
		info.add("Star Wars: Episode IV - A New Hope\t1977\t121\tIMDB");
		infoVer.add(new Movie("1977","Star Wars: Episode IV - A New Hope","121"));
		infoVer.get(1).setSource("IMDB");
		
		info.add("\t\t90\t");
		infoVer.add(new Movie("","","90"));
		
		info.add("d\t\t90\t");
		infoVer.add(new Movie("","d","90"));
		
		//makes sure it can convert both normal and unusual entries to Movies
		//I put a runtime otherwise the .equals() fails since the runtime must be 
		//turned into an integer
		//since it is being provided what would be processed date the column numbers are
		//just it's position in the toString() of the Movie class
		assertEquals(infoVer, Utilities.convertToMovie(info, 2, 1, 3, "IMDB", 4));
	}
	@Test
	void testConvertToMovie() {
		ArrayList<String> info = new ArrayList<String>();
		ArrayList<Movie> infoVer = new ArrayList<Movie>();
		
		info.add("Shrek\t2001\t95\tIMDB");
		infoVer.add(new Movie("2001","Shrek","95"));
		infoVer.get(0).setSource("IMDB");
		
		info.add("Star Wars: Episode IV - A New Hope\t1977\t121\tIMDB");
		infoVer.add(new Movie("1977","Star Wars: Episode IV - A New Hope","121"));
		infoVer.get(1).setSource("IMDB");
		
		info.add("\t\t90\t");
		infoVer.add(new Movie("","","90"));
		
		info.add("d\t\t90\t");
		infoVer.add(new Movie("","d","90"));
		
		//same test cases as above but this method is meant to be used on processed arrays
		//so it does not need the parameters other than the ArrayList
		assertEquals(infoVer, Utilities.convertToMovie(info));
	}
	@Test
	void testConvertMovieToInfo() {
		ArrayList<Movie> info = new ArrayList<Movie>();
		ArrayList<String> infoVer = new ArrayList<String>();
		
		infoVer.add("Shrek\t2001\t95\tIMDB");
		info.add(new Movie("2001","Shrek","95"));
		info.get(0).setSource("IMDB");
		
		infoVer.add("Star Wars: Episode IV - A New Hope\t1977\t121\tIMDB");
		info.add(new Movie("1977","Star Wars: Episode IV - A New Hope","121"));
		info.get(1).setSource("IMDB");
		
		infoVer.add("\t\t90\t");
		info.add(new Movie("","","90"));
		info.get(2).setSource("");
		
		infoVer.add("d\t\t90\t");
		info.add(new Movie("","d","90"));
		info.get(3).setSource("");
		
		//makes sure it is able to convert the ArrayList back to String from Movie
		assertEquals(infoVer, Utilities.convertMovieToInfo(info));
		
	}

}
