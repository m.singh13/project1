package movies.tests;
import movies.importer.Movie;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
/**
 *  
 * @author Mandeep Singh Mishal Alarifi
 * 
 * Test cases for the Movie class
 *
 */

class MovieTest {

	
	//Making sure Movie Object is properly created
	@Test
	void testMovieConstructor() {
		Movie shrek = new Movie("2001","Shrek","1h35m");
		assertEquals("2001",shrek.getReleaseYear());
		assertEquals("Shrek",shrek.getName());
		assertEquals("1h35m",shrek.getRuntime());
	}
	
	//Making sure setSource() works properly
	@Test
	void testMovieSource() {
		Movie shrek = new Movie("2001","Shrek","1h35m");
		shrek.setSource("Path A");
		assertEquals("Path A",shrek.getSource());
	}
	
	//Making sure toString() works properly
	@Test
	void testMovietoString() {
		Movie shrek = new Movie("2001","Shrek","1h35m");
		shrek.setSource("Path A");
		assertEquals("Shrek\t2001\t1h35m\tPath A",shrek.toString());

	}
	
	//Making sure equals() works properly
	@Test
	void testEquals() {
		//normal check to make sure that it works for normal values
		Movie test = new Movie("2001", "Shrek", "90");
		Movie testVer = new Movie("2001", "Shrek", "90");
		assertEquals(true, test.equals(testVer));
		//makes sure that if release years aren't the same it returns false
		test = new Movie("201", "Shrek", "90");
		testVer = new Movie("2001", "Shrek", "90");
		assertEquals(false, test.equals(testVer));
		//makes sure that they are equal with 5 min below the other
		test = new Movie("2001", "Shrek", "85");
		testVer = new Movie("2001", "Shrek", "90");
		assertEquals(true, test.equals(testVer));
		//makes sure that they are equal with 5 min above the other
		test = new Movie("2001", "Shrek", "95");
		testVer = new Movie("2001", "Shrek", "90");
		assertEquals(true, test.equals(testVer));
		//makes sure that if titles aren't the same it returns false
		test = new Movie("2001", "Srek", "95");
		testVer = new Movie("2001", "Shrek", "90");
		assertEquals(false, test.equals(testVer));
		
	}

}
