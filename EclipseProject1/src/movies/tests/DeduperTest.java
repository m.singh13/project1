package movies.tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.Deduper;

/**
 * 
 * @author Mishal Alarifi
 *
 */

class DeduperTest {

	@Test
	void test() {
		Deduper test = new Deduper("/home/mishal/project1/testfiles", "/home/mishal");
		ArrayList<String> info = new ArrayList<String>();
		ArrayList<String> infoVer = new ArrayList<String>();
		
		info.add("Miss Jerry\t1894\t45\tIMDB");
		
		info.add("Miss Jerry\t1894\t50\tIMDB");
		
		info.add("Miss Jerry\t1894\t55\tIMDB");
		
		info.add("Miss Jerry\t1894\t60\tIMDB");
		
		info.add("Miss Jerry\t1894\t65\tIMDB");
		
		info.add("Miss Jerry\t1894\t70\tIMDB");
		
		info.add("Not Miss Jerry\t1894\t45\tKaggle");
		
		info.add("Not Miss Jerry\t1894\t45\tIMDB");
		
		infoVer.add("Miss Jerry\t1894\t70\tIMDB");
		infoVer.add("Not Miss Jerry\t1894\t45\tIMDB:Kaggle");
		
		//makes sure that incrementing runtimes are grouped and runtimes within 5 min are grouped.
		//also makes sure that duplicate entries with the same source are merged with that source
		//Makes sure that if duplicate entries have different sources then they are concatonated in the merged entry
		assertEquals(infoVer, test.process(info));
	}
}
