package movies.tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.Validator;
/**
 * 
 * @author Mishal Alarifi
 *
 */

class ValidatorTest {

	@Test
	void testProcess() {
		Validator test = new Validator("/home/mishal/project1/testfiles", "/home/mishal");
		ArrayList<String> info = new ArrayList<String>();
		ArrayList<String> infoVer = new ArrayList<String>();
		//normal entry to make sure it works for normal values
		info.add("Miss Jerry\t1894\t45\tIMDB");
		infoVer.add("Miss Jerry\t1894\t45\tIMDB");
		//testing with a null entry
		info.add("Miss Jerry\t\t45\tIMDB");
		//testing with an invalid runtime (can't convert to int)
		info.add("Miss Jerry\t1894\thel\tIMDB");
		//another normal entry to make sure it works regardless of how many normal entries are inbetween
		info.add("Miss Jerry\t1894\t45\tIMDB");
		infoVer.add("Miss Jerry\t1894\t45\tIMDB");
		//testing with invalid year (can't convert to int)
		info.add("Miss Jerry\tdub\t45\tIMDB");
		//testing without values
		info.add("\t\t\t");
		//testing without title
		info.add("\t1894\t45\tIMDB");
		//testing with null values
		info.add(null+"\t"+null+"\t"+null+"\t"+null);
		//another normal entry to make sure it works when sandwiched
		info.add("Miss Jerry\t1894\t45\tIMDB");
		infoVer.add("Miss Jerry\t1894\t45\tIMDB");
		
		assertEquals(infoVer, test.process(info));
	}
}
