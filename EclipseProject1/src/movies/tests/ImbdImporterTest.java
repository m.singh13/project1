package movies.tests;

import static org.junit.jupiter.api.Assertions.*;


import java.io.IOException;
import java.util.ArrayList;

import movies.importer.ImdbImporter;
import org.junit.jupiter.api.Test;

/**
 * 
 * @author Mishal Alarifi
 *
 */

class ImbdImporterTest {

	@Test
	void test() throws IOException {
		ImdbImporter test = new ImdbImporter("/home/mishal/project1/testfiles", "/home/mishal");
		ArrayList<String> info = new ArrayList<String>();
		ArrayList<String> infoVer = new ArrayList<String>();
		
		info.add("tt0000009	Miss Jerry	Miss Jerry	1894	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	\"Blanche Bayliss, William Courtenay, Chauncey Depew\"	The adventures of a female reporter in the 1890s.	5.9	154					1	2\n");
		infoVer.add("Miss Jerry\t1894\t45\tIMDB");
		
		info.add("tt0002199	\"From the Manger to the Cross; or, Jesus of Nazareth\"	\"From the Manger to the Cross; or, Jesus of Nazareth\"	1912	1913	\"Biography, Drama\"	60	USA	English	Sidney Olcott	Gene Gauntier	Kalem Company	\"R. Henderson Bland, Percy Dyer, Gene Gauntier, Alice Hollister, Samuel Morgan, James D. Ainsley, Robert G. Vignola, George Kellog, J.P. McGowan\"	\"An account of the life of Jesus Christ, based on the books of the New Testament: After Jesus' birth is foretold to his parents, he is born in Bethlehem, and is visited by shepherds and wise...\"	5.7	484					13	5\t");
		
		info.add("tt0002199	\"From the Manger to the Cross; or, Jesus of Nazareth\"	\"From the Manger to the Cross; or, Jesus of Nazareth\"	1912	1913	\"Biography, Drama\"	60	USA	English	Sidney Olcott	Gene Gauntier	Kalem Company	\"R. Henderson Bland, Percy Dyer, Gene Gauntier, Alice Hollister, Samuel Morgan, James D. Ainsley, Robert G. Vignola, George Kellog, J.P. McGowan\"	\"An account of the life of Jesus Christ, based on the books of the New Testament: After Jesus' birth is foretold to his parents, he is born in Bethlehem, and is visited by shepherds and wise...\"	5.7	484					13	5\t");

		
		info.add("tt0000009	Miss Jerry	Miss Jerry	1894	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	\"Blanche Bayliss, William Courtenay, Chauncey Depew\"	The adventures of a female reporter in the 1890s.	5.9	154					1	\n");
		infoVer.add("Miss Jerry\t1894\t45\tIMDB");
		
		info.add("tt0002199	\"From the Manger to the Cross; or, Jesus of Nazareth\"	\"From the Manger to the Cross; or, Jesus of Nazareth\"	1912	1913	\"Biography, Drama\"	60	USA	English	Sidney Olcott	Gene Gauntier	Kalem Company	\"R. Henderson Bland, Percy Dyer, Gene Gauntier, Alice Hollister, Samuel Morgan, James D. Ainsley, Robert G. Vignola, George Kellog, J.P. McGowan\"	\"An account of the life of Jesus Christ, based on the books of the New Testament: After Jesus' birth is foretold to his parents, he is born in Bethlehem, and is visited by shepherds and wise...\"	5.7	484					13	5\t");

		
		//makes sure that normal entries are correctly dealt with and invalid entries (wrong number of columns) is discarded
		//makes sure that it behaves properly if invalid entries are sandwiched between valid ones and if there are
		//two invalid entries in a row
		assertEquals(infoVer,test.process(info));
	}
}
